# Private Layer
## Objetivo
Organizar e distribuir códigos comuns a vários componentes

## Estrutura
O package encontra-se na pasta 'src', chamado de 'private_layer' e é divido em dois submódulos, 'aws' e 'utils'.

### AWS
Neste submódulo encontram-se o conteúdo AWS. Subentende-se por conteúdo AWS qualquer código que exija a utilização das library 'boto3' ou 'botocore'.
#### Conteúdo
- dynamo
- glue
- rds
- s3
- sm

### Utils
Neste submódulo encontra-se qualquer outro código auxiliar independente, criados apenas com as libraries internas, ou seja, módulos contidos na própria distribuição Python.
#### Conteúdo
- check_delimiter
- date_parser
- get_uuid
- time_it

##### Observação
Caso seja necessário adicionar outros códigos dependentes de libs, frameworks ou serviços externos - como Pandas, Flask ou Spark -, recomenda-se a criação de diretório próprio, utilizando-se o nome da lib necessária como novo submódulo.

## Distribuição
A distribuição dos códigos é feita em .WHL e .ZIP, ambos no diretório 'dist'. O WHL é o instalador comum do Python, enquanto o ZIP é compatível apenas com layers do AWS Lambda.

### Obs
Ambos devem ser disponibilizados no bucket s3-naturaeco-us-east-1-adapter-codes-dev:
* O .ZIP deverá ser colocado no diterório 'libs/private/';
* O .WHL, diretório 'whl/'.

### Build
Para criar a distribuição é necessário os pacotes 'wheel' e 'check-wheel-contents' instalados.

`pip install wheel check-wheel-contents`

Após a instalação dos mesmos, basta ir até o diretório 'private_layer' e criar sua distribuição, disponibilizadas no diretório 'dist' da mesma.

`cd private_layer`

`python setup.py bdist_wheel`

## Utilização
A utilização desse módulo segue o processo comum de qualquer outro.

## Tests
Ainda não disponível

# Controle de versionamento
O controle de versionamento se dará da seguinte forma:
* A versão será formada por três números no formato "X.Y.Z";
* O X representa mudança de compatibilidade com versões anteriores;
* O Y representa adição de novas funções;
* O Z representa mudanças em funções, seja o conserto ou adição de funcionalidade.

e.g: 0.1.4

## Cuidado
Qualquer mudança nos parâmetros de entrada e saída de uma função podem gerar incompatibilidade, assim sendo, a refatoração e/ou otimização de um código implica apenas em mudança interna da mesma.

