import pytest
from src.private_layer.utils import date_parser


def test_always_true():
    assert True


@pytest.mark.parametrize('date, parsed_date', [
    (
        "20211201",
        ["20211201"]
    ),  # Always return a list
    (
        "20211201-20211203",
        ["20211203", "20211202", "20211201"]
    ),  # Must return ordered range of data
    (
        "20211201-20211203,20211202",
        ["20211203", "20211202", "20211201"]
    ),  # None duplicates
    (
        "20211201-20211203,20211202-20211204",
        ["20211204", "20211203", "20211202", "20211201"]
    ),  # Concatenate both ranges without duplicates
    (
        "20211215,20211203,20211201,20211202,20211204",
        ["20211215", "20211204", "20211203", "20211202", "20211201"]
    ),  # Return ordered even if input none range
    (
        "202155", ['20210505']
    )

])
def test_output(date, parsed_date):
    assert date_parser.date_parser(date) == parsed_date
