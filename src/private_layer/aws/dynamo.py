#%%
# Built-in
import logging
from re import subn as re_subn

# Third Party
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key

# Modules
from ..utils.itertools import flatify, iter_chain, count_elements

logger = logging.getLogger()
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# ch.setLevel(logging.INFO)
# formatter = logging.Formatter(
#     "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
# )
# ch.setFormatter(formatter)
# logger.addHandler(ch)


def error_handling(error):
    """
        Universal error handling for Boto 1.20.24 as described at the
        official documentation.
        https://boto3.amazonaws.com/v1/documentation/api/latest/guide/error-handling.html
    """
    error_code = error.response['Error']['Code']
    error_msg = error.response['Error']['Message']
    reponse_code = error.response['ResponseMetadata']['HTTPStatusCode']

    msg = f'Error "{error_code}": {error_msg}'
    logger.error(msg)

    return (reponse_code, msg)


def get_all_items(table_name: str) -> tuple:
    """
        Return all items by given table's name.
    """
    try:
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        response = table.scan()
        items = response['Items']
    except ClientError as error:
        return error_handling(error)
    else:
        return (200, items)


def get_item_by_key(table_name: str, key: dict) -> dict:
    """
        Return item by given table's name and key.

    """
    try:
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        response = table.get_item(Key=key)
        items = response['Item']
    except ClientError as error:
        return error_handling(error)
    except KeyError as error:
        # Not found
        return (404, None)
    else:
        return (200, items)


def get_item_by_sk_prefix(
    pk_name: str,
    table_name: str,
    pk_value: str,
    sk_name: str,
    sk_value: str
) -> list:
    """
        Return item by given table's name and: sk_value, pk_value, sk_name and
    sk_value.
        Even if has parcial sort key.
    """
    try:
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        response = table.query(
            KeyConditionExpression=Key(pk_name).eq(pk_value) &
            Key(sk_name).begins_with(sk_value)
        )
        items = response['Items']
    except ClientError as error:
        return error_handling(error)
    else:
        return (200, items)


def put_item_by_key(table_name: str, key: dict) -> dict:
    """
        Add or Put some item given table's name and a key with the fields to be
    created or updated.
    """
    try:

        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)
        response = table.put_item(Item=key)
        items = response

    except ClientError as error:
        return error_handling(error)
    else:
        return (200, items)


def update_item_by_key(
    table_name: str,
    key: dict,
    update: dict,
    **kwargs
) -> tuple:
    """
    Args:
        table_name (str): Table name
        key (dict): Partition and sort key
        update (dict): Dictionary containing path and value to be updated
        kwargs: Any other update_item params except "Key",
    "ExpressionAttributeNames", "ExpressionAttributeValues", and
    "UpdateExpression"

    Returns (tuple):
        HTTPStatusCode (int): Response status code from dynamo
        Message (str or dict): Message with info from dynamo
    """

    # Transform update param in "tuple: value"
    logger.debug('Flatifing of entry')
    update = flatify(update)

    # Count elements and check tail of each branch
    logger.debug('Counting elements and unique values')
    elements = count_elements(iter_chain(update.keys()))
    unique_values = set([k[-1] for k in update.keys()])
    elements = {
        k: (1 if k not in unique_values else v)
        for k, v in elements.items()
    }

    # Forbidden characters
    def sanitize(string: str) -> str:
        """
        Substitute non alpha-numeric except underscore by uderscore
        """
        return re_subn('[^a-zA-Z0-9_]', '_', string)[0]

    # Generate expression attribute names
    logger.debug('Generating expression attribute names')
    expr_attrib_names = dict()
    _elements = elements.copy()
    for k in iter_chain(update.keys()):
        if _elements[k] == 1 and elements[k] == 1:
            expr_attrib_names[f'#{sanitize(k)}'] = k
        else:
            _elements[k] -= 1
            expr_attrib_names[
                f'#{sanitize(k)}{_elements[k]}'] = k

    # Generate expression attribute values
    logger.debug('Generating expression attribute values')
    expr_attrib_values = dict()
    _elements = elements.copy()
    for k, v in update.items():
        k = k[-1]
        if _elements[k] == 1 and elements[k] == 1:
            expr_attrib_values[f':{sanitize(k)}'] = v
        else:
            _elements[k] -= 1
            expr_attrib_values[
                f':{sanitize(k)}{_elements[k]}'] = v

    # Generate update expression
    logger.debug('Generating update expression')
    update_expr = list()
    v_elements = elements.copy()
    k_elements = elements.copy()
    for k in update.keys():
        _temp_expr = list()
        for s in k:
            if k_elements[s] == 1 and elements[s] == 1:
                _temp_expr.append(f'#{sanitize(s)}')
            else:
                k_elements[s] -= 1
                _temp_expr.append(
                    f'#{sanitize(s)}{k_elements[s]}')
        else:
            if v_elements[k[-1]] == 1 and elements[k[-1]] == 1:
                _value = f':{sanitize(k[-1])}'
            else:
                v_elements[k[-1]] -= 1
                _value = f':{sanitize(k[-1])}{v_elements[k[-1]]}'
            update_expr.append(f"{'.'.join(_temp_expr)} = {_value}")
    else:
        update_expr = ', '.join(update_expr)

    # Mount arguments to send to Dynamo
    logger.debug('Mounting arguments to DynamoDB')
    args = {
        "Key": key,
        "ExpressionAttributeNames": expr_attrib_names,
        "ExpressionAttributeValues": expr_attrib_values,
        "UpdateExpression": f"SET {update_expr}"
    }

    logger.debug(args)
    for k in args.keys():
        try:
            kwargs.pop(k)
            logger.warn(f'Incompatible kwarg item removed: "{k}"')
        except KeyError:
            pass
    else:
        args.update(kwargs)

    # Connect to DynamoDB and send request
    logger.debug('Start connection to DynaomDB')
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(table_name)
    try:
        logger.debug(f'Trying to update item in {table_name} table')
        response = table.update_item(**args)
        logger.debug('Update successfully')
        items = response['ResponseMetadata']
    except ClientError as error:
        return error_handling(error)
    else:
        return (200, items)
