import boto3
from botocore.exceptions import ClientError

def s3_open_file(bucket: str, key: str, show_length: bool = False) -> object:
    """
    args:
        - bucket (str): bucket name  
        - key (str): path until file
    return:
        - object: botocore.response.StreamingBody,
        - int: size of the body in bytes
    """
    client = boto3.client('s3')
    file = client.get_object(
        Bucket=bucket,
        Key=key
    )
    if show_length:
        return (file['Body'], int(file['ContentLength']))
    else:
        return file['Body']

def s3_get_file_key(
        bucket_name: str,
        key_path: str = '',
        list_all: bool = False
    ) -> list or tuple:
    """
    args:
        - bucket_name (str): bucket name  
        - key_path (str): path until file
        - list_all (bool): check
    return:
        - tuple with key and size
        - list of tuples with key and size

    """
    client = boto3.client('s3')
    s3_keys = list()
    s3_reponse = client.list_objects(
        Bucket=bucket_name,
        Prefix=key_path,
        Delimiter='/'
    )
    for obj in s3_reponse.get('Contents', list()):
        key = obj.get('Key', '')
        size = obj.get('Size', '')
        s3_keys.append((key, size))
        if not list_all:
            break
    if list_all:
        return s3_keys
    else:
        return s3_keys[0]