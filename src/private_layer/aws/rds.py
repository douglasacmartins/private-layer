import boto3
import os

#TODO: Config function
def connect_aurora_integration_layer(system,system_type,entity):
    client = boto3.client("rds-data")
    secretArn = arn_secret_manager # variavel configurada fora da função
    resourceArn = arn_rds # variavel configurada fora da função
    database='postgres'
    if entity==None:
        return client.execute_statement(\
            secretArn=secretArn,\
            database=database,\
            resourceArn=resourceArn,\
            sql=f"select * from integration_layer.system where system_dc ='{system}'")
    elif entity!=None and system_type=='sftp':
        return client.execute_statement(\
            secretArn=secretArn,\
            database=database,\
            resourceArn=resourceArn,\
            sql=f"select * from integration_layer.entity_sftp where system_cd ='{system}' and entity_dc = '{entity}'")
