import logging
import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def start_job_run(glue_job_name: str, glue_job_args: dict = dict()) -> str:
    glue = boto3.client('glue')
    try:
        response = glue.start_job_run(
            JobName = glue_job_name,
            Arguments = glue_job_args
        )
    except ClientError as e:
        response = e.response['Error']['Code']
        if e.response['Error']['Code'] == 'ConcurrentRunsExceededException':
            logger.error(f'{response}: Concurrent runs exceeded for "{glue_job_name}"')
        else:
            raise e
    else:
        logger.info(f'Job Run Id: {response["JobRunId"]}')
        return response["JobRunId"]