import paramiko


class FastTransport(paramiko.Transport):
    def __init__(self, sock):
        super(FastTransport, self).__init__(sock)
        self.window_size = 2147483647
        self.packetizer.REKEY_BYTES = pow(2, 40)
        self.packetizer.REKEY_PACKETS = pow(2, 40)


def sftp_connect(host, port, username, pw):
    try:
        transport = FastTransport((host, port))
        transport.connect(username=username, password=pw)
        conn = paramiko.SFTPClient.from_transport(transport)
        return conn
    except Exception as e:
        print("Failed on exception", repr(e))
        raise
