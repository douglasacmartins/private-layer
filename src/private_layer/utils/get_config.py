import configparser as cp
from ..aws.s3 import s3_open_file


class ConfigVariable(object):
    def __init__(self):
        # location in s3 of file application.cfg
        configuration_file_bucket = "s3-naturaeco-us-east-1-adapter-codes-dev"
        configuration_file_key = "adapters/application.cfg"

        # content inside file application.cfg
        file = ''

        # object RawConfigParser
        props = ''

        try:
            # initialize object
            props = cp.RawConfigParser()
        except Exception as e:
            print(
                f'Error in RawConfigParser: {e.__str__()}\n'
                f'{repr(e)}'
            )

        try:
            # get file from s3
            obj = s3_open_file(
                bucket=configuration_file_bucket,
                key=configuration_file_key
            )

            # read file
            file = obj.read().decode()
        # TODO: Tratar exceção no s3_open_file
        except Exception as e:
            print(
                f'Error while reading {configuration_file_key} '
                'from {configuration_file_bucket}: {e.__str__()}\n'
                f'{repr(e)}'
            )

        try:
            props.read_string(file)
        except Exception as e:
            print(
                f'Error while parsing application.cfg: {e.__str__()}\n'
                f'{repr(e)}'
            )

        # Environment with DEFAULT in file application.cfg
        environment = props.get("DEFAULT", "environment")
        try:
            items = props.items(environment)
            for k, v in items:
                setattr(self, k, v)
        except Exception as e:
            print(
                f'Error while self attribute setting: {e.__str__()}\n'
                f'{repr(e)}'
            )
