#%%
from time import time_ns

class TimeIt(object):
    '''
        With timer
    '''
    def __enter__(self):
        self.start_time = time_ns()
        return self
    
    def __exit__(self, *args):
        self.end_time = time_ns()
    
    @property
    def time_ns(self):
        return self.end_time - self.start_time
    
    @property
    def time_s(self):
        return round(self.time_ns*1e-9,3) 